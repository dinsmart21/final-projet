<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    Protected $table = 'genre';
    Protected $fillable = ['genre'];
}
