@extends('layout.master')
@section('judul')
Detail Film {{$film->judul}}
@endsection



@section('content')

<img src="{{asset('poster/'.$film->poster)}}" alt="">
<h4>{{$film->judul}}</h4>
<p>{{$film->ringkasan}}</p>

<a href="/film" class="btn btn-secondary">Kembali</a>
    


@endsection