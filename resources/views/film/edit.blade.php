@extends('layout.master')
@section('judul')
Edit Film
@endsection

@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Judul Film</label>
        <input type="text" class="form-control" value="{{$film->judul}}" name="judul" placeholder="Masukkan Title">
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Ringkasan</label>
        <input type="text" class="form-control" name="ringkasan" placeholder="Masukkan Title">
        @error('ringkasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <textarea name="tahun" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>

        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>


    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" id="" class="form-control">
            <option value="">--Pilih Genre--</option>
            @foreach ($genre as $item)

                @if ($item->id === $film->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}" >{{$item->nama}}</option>
                @endif


            @endforeach
        </select>
        @error('genre')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>


    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster">
        @error('poster')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    
    <button type="submit" class="btn btn-primary">Edit</button>
</form>





@endsection