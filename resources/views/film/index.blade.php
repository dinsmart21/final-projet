@extends('layout.master')
@section('judul')
Daftar Film
@endsection

@section('content')

@auth
<a href="/film/create" class="btn btn-info btn-dark"> Tambah Film</a>
@endauth


<div class="row mt-3">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                
                <img src="{{asset('poster/'.$item->poster)}}" class="card-img-top " alt="...">
                <div class="card-body">
                    <h3 class="mt-2">{{($item->judul)}}</h3> 
                {{-- <p class="card-text">{{Str::limit ($item->ringkasan, 20)}}</p> --}}
                @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('Delete')
                    
                <a href="/film/{{$item->id}}/edit" class="btn btn-dark btn-sn">Edit</a>
                <input type="submit" class="btn btn-dark btn-sn" value="Delete">  
                @endauth
                <a href="/film/{{$item->id}}" class="btn btn-dark btn-sn">Detail</a>
             </div>
        </div>  
</div>
@empty
    <h1> Data Film Masih Kosong</h1>
@endforelse
</div>


@endsection