@extends('layout.master')
@section('judul')
Daftar Genre
@endsection

@section('content')

<a href="genre/create" class="btn btn-info btn-dark my-2"> Tambah Genre</a>

<div class="row">
    @forelse ($genre as $item)
        <div class="col-4 mt-3">
            <div class="card">    
                <h3 class="ml-4 mt-2">{{($item->nama)}}</h3>  
                <div class="card-body">
                
                <form action="/genre/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/genre/{{$item->id}}" class="btn btn-dark btn-sn">Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-dark btn-sn">Edit</a>
               <input type="submit" class="btn btn-dark btn-sn" value="Delete">
             </div>
        </div>  
</div>
@empty
    <h1> Data Genre Masih Kosong</h1>
@endforelse
</div>


@endsection