@extends('layout.master')
@section('judul')
Edit Genre
@endsection

@section('content')

<form action="/genre/{{$genre->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Genre</label>
        <input type="text" class="form-control" value="{{$genre->nama}}" name="nama" placeholder="Masukkan Genre">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror    
    </div>


    
    <button type="submit" class="btn btn-dark">Edit</button>
</form>





@endsection